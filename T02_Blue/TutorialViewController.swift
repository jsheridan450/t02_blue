//
//  TutorialViewController.swift
//  T02_Blue
//
//  Created by Oscar Tapia on 11/28/18.
//  Copyright © 2018 Team Blue. All rights reserved.
//

import UIKit
import FirebaseAuth

class TutorialViewController: UIViewController {
    var db: InventoryDatabase? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.setNavigationBarHidden(true, animated: animated)
    }

    @IBOutlet weak var pageControl: UIPageControl!
}

extension TutorialViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.bounds.width
        pageControl.currentPage = Int(scrollView.contentOffset.x / pageWidth)
    }
}
